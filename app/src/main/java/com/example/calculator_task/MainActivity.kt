package com.example.calculator_task

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast


class MainActivity : AppCompatActivity() {

    private var tempNumb: Double? = null
    private var operation: Char = '.' // Default value

    private lateinit var resultTextView: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTextView = findViewById(R.id.resultTextView)


    }

    fun num_Click(clickedView: View) {
        if (clickedView is TextView) {
            if (resultTextView.text.length == 0 && clickedView.text.toString() == ".") {
                return
            } else if (clickedView.text.toString() == "." && resultTextView.text.contains(".")) {
                return
            } else if (resultTextView.text.toString() == "0" && clickedView.text.toString() != ".") {
                resultTextView.text = ""
            }
            resultTextView.append(clickedView.text)
        }
    }

    fun clearAll(clickedView: View) {
        resultTextView.text = ""
        defaultState()
    }

    fun del(clickedView: View) {
        var text = resultTextView.text.toString()
        if (text.isNotEmpty()) {
            text = text.dropLast(1)
        }
        resultTextView.text = text
    }

    fun doOperation(clickedView: View) {
        if (clickedView is TextView) {
            if (operation != '.') {
                Toast.makeText(
                    this,
                    "Please evaluate(Press '=') your current expression first.",
                    Toast.LENGTH_SHORT
                ).show()
                return
            }
            if (resultTextView.text.length == 0) {
                Toast.makeText(this, "Please enter the number", Toast.LENGTH_SHORT).show()
                return
            }
            tempNumb = resultTextView.text.toString().toDouble()
            resultTextView.text = ""
            operation = clickedView.text[0]
        }
    }

    fun equalsBtn(clickedView: View) {
        if (clickedView is TextView) {
            if (operation == '.') {
                return
            }
            if (resultTextView.text.isEmpty()) {
                Toast.makeText(this, "Please enter second number", Toast.LENGTH_SHORT).show()
                return
            }
            val secondTempNumb = resultTextView.text.toString().toDouble()
            when (operation) {
                '+' -> tempNumb = tempNumb?.plus(secondTempNumb)
                '-' -> tempNumb = tempNumb?.minus(secondTempNumb)
                '*' -> tempNumb = tempNumb?.times(secondTempNumb)
                '/' -> tempNumb = tempNumb?.div(secondTempNumb)
            }
            resultTextView.text = tempNumb.toString()

            defaultState()
        }
    }

    private fun defaultState() {
        tempNumb = null
        operation = '.'
    }

}